**Первая часть тестового задания. Креонит.**

1.	Сверстать макет главной страницы (maxima_home) нашего старого проекта Фитнес Клуб Максима – http://fcmaxima.ru/ 
2.	Использовать flexbox, transition

Макеты:
https://drive.google.com/drive/folders/0B95aRdPG1z6BcHkzdlZIT1J2M0U


**Требования.**
1.	Верстать кроссбраузерно (ie11, edge, chrome последняя версия, safari последняя версия), PixelPerfect. 
2.	Без Bootstrap и других сторонних библиотек, только ручная верстка. 
3.	Писать код по БЭМу.
4.	Желательно использовать препроцессор и сборщик css файлов
5.	Для js обязательно использовать сторонние библиотеки. 
6.	Залить на gitlab в приватный репозиторий и выслать инвайт пользователя anton_creonit
