var meter = $(".footerInfo__meter").html();
var calorie = +$(".footerInfo__calorie").html();

function Distance(){
  $(".footerInfo__meter").html(++meter);
}

function Calories(){
  calorie += 0.09;
  $(".footerInfo__calorie").html(calorie.toFixed(2));
}

function WalkingMan(){
  if ($(".footerInfo__walkingMan").hasClass("footerInfo__walkingMan--active_right")) {
    $(".footerInfo__walkingMan").removeClass('footerInfo__walkingMan--active_right');
    $('.footerInfo__walkingMan').toggleClass('footerInfo__walkingMan--active_left');
  } else {
    $(".footerInfo__walkingMan").removeClass('footerInfo__walkingMan--active_left');
    $('.footerInfo__walkingMan').toggleClass('footerInfo__walkingMan--active_right');
  }
}

function Skier(){
  $(".events__skier").toggleClass("events__skier--active");
}

function SkierRepeat(){
  $(".events__skier").removeClass('events__skier--active');
}

$(document).ready(function(){
  $("input[name='weight']").inputmask({"mask": "9{1,3} кг"}); //шаблоны для ввода веса, возраста и роста
  $("input[name='age']").inputmask({"mask": "9{1,2} лет"});
  $("input[name='height']").inputmask({"mask": "9{1,3} см"});
  WalkingMan();
  Skier();
  setInterval(Skier, 6000);
  setInterval(SkierRepeat, 5990);
  setInterval(Distance, 1000);
  setInterval(Calories, 1000);
  setInterval(WalkingMan, 10000);
});